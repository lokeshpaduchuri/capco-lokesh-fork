import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class GetJsonDataService {

  constructor(private http: HttpClient) { }

  getJsonData() {
    return this.http.get('./assets/sample_data.json');
  }

  postJsonData(url, data) {
    this.http.post(url, data);
  }
}
