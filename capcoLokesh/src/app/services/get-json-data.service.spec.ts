import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';

import { GetJsonDataService } from './get-json-data.service';

describe('GetJsonDataService', () => {
  beforeEach(() => TestBed.configureTestingModule({
    imports: [HttpClientTestingModule]
  }));

  it('should be created', () => {
    const service: GetJsonDataService = TestBed.get(GetJsonDataService);
    expect(service).toBeTruthy();
  });
});
