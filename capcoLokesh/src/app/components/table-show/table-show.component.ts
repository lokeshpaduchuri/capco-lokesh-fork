import { Component, OnInit } from '@angular/core';
import { PagerService } from '../../services/pager.service';
import { GetJsonDataService } from '../../services/get-json-data.service';
import { AppRoutingModule } from '../../app-routing.module';

@Component({
  selector: 'app-table-show',
  templateUrl: './table-show.component.html',
  styleUrls: ['./table-show.component.scss']
})
export class TableShowComponent implements OnInit {

  // saving the json object into this array
  public tableData: any[] = [];

  // slicing the tableData into chunks for pagination
  public tableRowsData: any[] = [];

  // this array consists of Table headers which is json keys
  public uniqueHeadersList: any[] = [];


  // Sample select options can be changes as per needed
  public selectOptions = [0, 10, 20, 30, 40, 50];

  // pager object
  pager: any = {};

  // paged items
  pagedItems: any[];

  // Counting the rows needed for table
  public noOfRows = 0;

  constructor(private pagerService: PagerService, private getJSONService: GetJsonDataService) { }

  ngOnInit(): void {
    // HTTP get request to get the sample data and saving values to tableData
    this.getJSONService.getJsonData().subscribe(res => {
      this.tableData = Object.values(res);

      // Setting the initial page with data
      this.setPageData(1);
     });
  }

  /**
   *
   * @param data input is tableData
   * @param this.uniqueHeadersList this returns the headerList
   */
  uniqueHeaders(data) {
    data.forEach(element => {
      // console.log(element);
      Object.keys(element).forEach(x => {
        if (!this.uniqueHeadersList.includes(x)) {
          this.uniqueHeadersList.push(x);
         }
      });
    });
  }

  /**
   *
   * @param columnName this is the column header
   * @param index value of the column
   */
  getColumnData(columnName, index) {
    const rowData = this.pagedItems[index][columnName];

    return rowData;
  }

  /**
   *
   * @param event which contains the No Of Rows value
   * @param noOfRows defines the number of rows in the table
   */
  setNoOfRows(event) {
    this.noOfRows = event.target.value;
    this.pagedItems = this.tableData.slice(0, this.noOfRows);
    if (this.pagedItems) {
      this.uniqueHeaders(this.pagedItems);
    }
  }

  /**
   *
   * @param page a number based on the selection in the pagination component
   */
  setPageData(page: number) {
    if (page < 1 || page > this.pager.totalPages) {
      return;
    }

    // get pager object from service
    this.pager = this.pagerService.getPager(this.tableData.length, page, +this.noOfRows);

    // get current page of items
    this.pagedItems = this.tableData.slice(this.pager.startIndex, this.pager.endIndex + 1);
  }

  /**
   * used to post the data using HTTP Post
   * @param item the total row data
   */
  submitStatus(item) {
    const dataObj = {id: item.id , status: item.status };
    this.getJSONService.postJsonData('./assets/submitted_data.json', dataObj);
  }

}
