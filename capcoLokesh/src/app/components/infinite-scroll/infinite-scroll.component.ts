import { Component, OnInit } from '@angular/core';
import { GetJsonDataService } from '../../services/get-json-data.service';
import { AppRoutingModule } from '../../app-routing.module';
import { Router } from '@angular/router';

@Component({
  selector: 'app-infinite-scroll',
  templateUrl: './infinite-scroll.component.html',
  styleUrls: ['./infinite-scroll.component.scss']
})
export class InfiniteScrollComponent implements OnInit {

  public infiniteTableData: any[] = [];



  // this array consists of Table headers which is json keys
  public uniqueHeadersList: any[] = [];

  // value is populated when drowndown value is selected
  public noOfRows = 0;

  constructor(private getJSONData: GetJsonDataService) { }

  ngOnInit() {
    const apt = this.getJSONData.getJsonData().subscribe(res => {
      this.infiniteTableData = Object.values(res);
      this.uniqueHeaders(this.infiniteTableData);
     });
  }

  /**
   *
   * @param data input is tableData
   * @param this.uniqueHeadersList this returns the headerList
   */
  uniqueHeaders(data) {
    data.forEach(element => {
      // console.log(element);
      Object.keys(element).forEach(x => {
        if (!this.uniqueHeadersList.includes(x)) {
          this.uniqueHeadersList.push(x);
         }
      });
    });
  }

  /**
   *
   * @param columnName this is the column header
   * @param index value of the column
   */
  getColumnData(columnName, index) {
    const rowData = this.infiniteTableData[index][columnName];

    return rowData;
  }


}
