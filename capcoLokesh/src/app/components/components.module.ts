import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { InfiniteScrollComponent } from './infinite-scroll/infinite-scroll.component';
import { TableShowComponent } from './table-show/table-show.component';



@NgModule({
  declarations: [InfiniteScrollComponent, TableShowComponent],
  imports: [
    CommonModule
  ]
})
export class ComponentsModule { }
