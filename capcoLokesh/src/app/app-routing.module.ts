import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { InfiniteScrollComponent } from './components/infinite-scroll/infinite-scroll.component';
import { TableShowComponent } from './components/table-show/table-show.component';

const routes: Routes = [
  {path: '', component: TableShowComponent},
  {path: 'infiniteScroll', component: InfiniteScrollComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
